#!/bin/sh

if git clone --filter=blob:none --no-checkout https://fuchsia.googlesource.com/fuchsia .fuchsia; then

    function configure_git_repository {
        # Install message hook, and default push arguments.
        f="$(git rev-parse --git-dir)/hooks/commit-msg"
        mkdir -p "$(dirname $f)"
        curl -Lo "$f" https://gerrit-review.googlesource.com/tools/hooks/commit-msg
        chmod +x "$f"

        # Install pre-push hook.
        f="$(git rev-parse --git-dir)/hooks/pre-push"
        mkdir -p "$(dirname $f)"
        printf "#!/bin/sh\n$1/presubmit.sh" > "$f"
        chmod +x "$f"

        # Make `git pull`, `git push`, and `git rebase` work without args.
        git config branch.*.remote origin
        git config remote.origin.push HEAD:refs/for/master
    }

    (
        cd .fuchsia

        git sparse-checkout init --cone
        git sparse-checkout add "rustfmt.toml"
        git sparse-checkout add "src/graphics/lib/compute/forma"
        git sparse-checkout add "src/graphics/lib/compute/rive-rs"
        git sparse-checkout add "src/graphics/lib/compute/surpass"

        git checkout main
        configure_git_repository ..
    )

    ln -sf .fuchsia/rustfmt.toml rustfmt.toml
    ln -sf .fuchsia/src/graphics/lib/compute/forma forma
    ln -sf .fuchsia/src/graphics/lib/compute/rive-rs rive-rs
    ln -sf .fuchsia/src/graphics/lib/compute/surpass surpass
    configure_git_repository .
fi
