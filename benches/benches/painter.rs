// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

use criterion::{criterion_group, criterion_main, Criterion};
use forma::{
    AffineTransform, BlendMode, Color, Fill, GradientBuilder, GradientType, Image, Point, Style,
    Texture,
};
use std::sync::Arc;
use surpass::painter::painter_fill_at_bench;

fn create_image(width: usize, height: usize) -> Image {
    Image::from_linear_rgba(vec![[0.0; 4]; width * height].into(), width, height).unwrap()
}

const SIZE: u16 = 1024;

pub fn painter(c: &mut Criterion) {
    let mut group = c.benchmark_group("painter");

    let texture = Texture {
        transform: AffineTransform::default(),
        image: Arc::new(create_image(SIZE.into(), SIZE.into())),
    };
    let style = Style {
        is_clipped: false,
        fill: Fill::Texture(texture.clone()),
        blend_mode: BlendMode::Over,
    };
    group.bench_function("texture", |b| {
        b.iter(|| painter_fill_at_bench(SIZE.into(), SIZE.into(), &style))
    });

    let style = Style {
        is_clipped: false,
        fill: Fill::Solid(Color::default()),
        blend_mode: BlendMode::Over,
    };
    group.bench_function("solid_color", |b| {
        b.iter(|| painter_fill_at_bench(SIZE.into(), SIZE.into(), &style))
    });

    let mut gradient =
        GradientBuilder::new(Point { x: 0.0, y: 0.0 }, Point { x: SIZE.into(), y: SIZE.into() });

    gradient.r#type(GradientType::Linear);
    gradient.color_with_stop(Color::default(), 0.25);
    gradient.color_with_stop(Color::default(), 0.25);
    let style = Style {
        is_clipped: false,
        fill: Fill::Gradient(gradient.build().unwrap()),
        blend_mode: BlendMode::Over,
    };
    group.bench_function("gradient_linear", |b| {
        b.iter(|| painter_fill_at_bench(SIZE.into(), SIZE.into(), &style))
    });

    let mut gradient =
        GradientBuilder::new(Point { x: 0.0, y: 0.0 }, Point { x: SIZE.into(), y: SIZE.into() });
    gradient.r#type(GradientType::Radial);
    gradient.color_with_stop(Color::default(), 0.25);
    gradient.color_with_stop(Color::default(), 0.25);
    let style = Style {
        is_clipped: false,
        fill: Fill::Gradient(gradient.build().unwrap()),
        blend_mode: BlendMode::Over,
    };
    group.bench_function("gradient_radial", |b| {
        b.iter(|| {
            painter_fill_at_bench(SIZE.into(), SIZE.into(), &style);
        })
    });
    group.finish();
}

criterion_group!(benches, painter,);
criterion_main!(benches);
