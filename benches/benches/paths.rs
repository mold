// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion, Throughput};
use forma::GeomId;
use surpass::{LinesBuilder, PathBuilder, Point};

pub fn line_to(c: &mut Criterion) {
    let mut group = c.benchmark_group("line_to");
    for lines in [10, 1000] {
        group.throughput(Throughput::Elements(lines as u64));
        group.bench_with_input(BenchmarkId::from_parameter(lines), &lines, |b, &lines| {
            b.iter(|| {
                let mut builder = PathBuilder::new();

                for line in 0..lines {
                    builder
                        .line_to(Point::new(line as f32, if line % 2 == 0 { -1.0 } else { 1.0 }));
                }

                builder.build()
            });
        });
    }
    group.finish();
}

pub fn quad_to(c: &mut Criterion) {
    let mut group = c.benchmark_group("quad_to");
    for quads in [10, 1000] {
        group.throughput(Throughput::Elements(quads as u64));
        group.bench_with_input(BenchmarkId::from_parameter(quads), &quads, |b, &quads| {
            b.iter(|| {
                let mut builder = PathBuilder::new();

                for quad in 0..quads {
                    builder.quad_to(
                        Point::new(quad as f32 + 0.5, if quad % 2 == 0 { -1.0 } else { 1.0 }),
                        Point::new(quad as f32 + 1.0, 0.0),
                    );
                }

                builder.build()
            });
        });
    }
    group.finish();
}

pub fn cubic_to(c: &mut Criterion) {
    let mut group = c.benchmark_group("cubic_to");
    for cubics in [10, 1000] {
        group.throughput(Throughput::Elements(cubics as u64));
        group.bench_with_input(BenchmarkId::from_parameter(cubics), &cubics, |b, &cubics| {
            b.iter(|| {
                let mut builder = PathBuilder::new();

                for cubic in 0..cubics {
                    builder.cubic_to(
                        Point::new(cubic as f32, if cubic % 2 == 0 { -1.0 } else { 1.0 }),
                        Point::new(cubic as f32 + 1.0, if cubic % 2 == 0 { -1.0 } else { 1.0 }),
                        Point::new(cubic as f32 + 1.0, 0.0),
                    );
                }

                builder.build()
            });
        });
    }
    group.finish();
}

pub fn transform(c: &mut Criterion) {
    let mut group = c.benchmark_group("transform");
    for lines in [10, 1000] {
        group.bench_with_input(BenchmarkId::from_parameter(lines), &lines, |b, &lines| {
            let mut builder = PathBuilder::new();

            for line in 0..lines {
                builder.line_to(Point::new(line as f32, if line % 2 == 0 { -1.0 } else { 1.0 }));
            }

            let path = builder.build();

            b.iter(|| path.transform(&[0.5, 0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 1.0]));
        });
    }
    group.finish();
}

pub fn cubics_to_lines(c: &mut Criterion) {
    let mut group = c.benchmark_group("cubics_to_lines");
    for cubics in [10, 1000] {
        group.throughput(Throughput::Elements(cubics as u64));
        group.bench_with_input(BenchmarkId::from_parameter(cubics), &cubics, |b, &cubics| {
            let mut builder = PathBuilder::new();

            for cubic in 0..cubics {
                builder.cubic_to(
                    Point::new(cubic as f32, if cubic % 2 == 0 { -1.0 } else { 1.0 }),
                    Point::new(cubic as f32 + 1.0, if cubic % 2 == 0 { -1.0 } else { 1.0 }),
                    Point::new(cubic as f32 + 1.0, 0.0),
                );
            }

            let path = builder.build();

            b.iter(|| LinesBuilder::new().push_path(GeomId::default(), &path));
        });
    }
    group.finish();
}

pub fn translate_same_path(c: &mut Criterion) {
    let mut group = c.benchmark_group("translate_same_path");
    for paths in [10, 1000] {
        group.throughput(Throughput::Elements(paths as u64));
        group.bench_with_input(BenchmarkId::from_parameter(paths), &paths, |b, &paths| {
            let weight = 2.0f32.sqrt() / 2.0;
            let radius = 10.0;

            let mut builder = PathBuilder::new();

            builder.move_to(Point::new(radius, 0.0));
            builder.rat_quad_to(Point::new(radius, -radius), Point::new(0.0, -radius), weight);
            builder.rat_quad_to(Point::new(-radius, -radius), Point::new(-radius, 0.0), weight);
            builder.rat_quad_to(Point::new(-radius, radius), Point::new(0.0, radius), weight);
            builder.rat_quad_to(Point::new(radius, radius), Point::new(radius, 0.0), weight);

            let path = builder.build();

            let mut builder = LinesBuilder::new();

            b.iter(|| {
                for p in 0..paths {
                    builder.push_path(
                        GeomId::default(),
                        &path.transform(&[1.0, 0.0, p as f32 * 0.5, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]),
                    );
                }
            });
        });
    }
    group.finish();
}

criterion_group!(
    benches,
    line_to,
    quad_to,
    cubic_to,
    transform,
    cubics_to_lines,
    translate_same_path
);
criterion_main!(benches);
