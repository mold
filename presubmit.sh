#!/bin/sh
set -e  # Exit on first error.
cd $( dirname "${BASH_SOURCE[0]}" )

echo "Checking benches..."
RUSTFLAGS="-D warnings" cargo check -q -p benches
echo "Checking demo..."
RUSTFLAGS="-D warnings" cargo check -q -p demo
echo "Checking conveyor-sort..."
RUSTFLAGS="-D warnings" cargo check -q -p conveyor-sort
echo "Checking painter..."
RUSTFLAGS="-D warnings" cargo check -q -p painter
echo "Checking pristine..."
RUSTFLAGS="-D warnings" cargo check -q -p pristine
echo "Checking renderer..."
RUSTFLAGS="-D warnings" cargo check -q -p renderer
echo "Checking web..."
(
    cd web 
    RUSTFLAGS="-D warnings -C target-feature=+simd128,+atomics,+bulk-memory,+mutable-globals" wasm-pack -q build --target web
)
echo "Testing conveyor-sort..."
cargo test -q -p conveyor-sort
echo "Testing painter..."
cargo test -q -p painter
echo "Testing renderer..."
cargo test -q -p renderer
echo "Checking formatting..."
cargo fmt -q --check
echo "Checking docs..."
RUSTDOCFLAGS="-D warnings" cargo doc -q -p forma
echo "Running forma doc tests..."
cargo test -q --doc -p forma
echo
echo "Pre-submit passed successfully."
